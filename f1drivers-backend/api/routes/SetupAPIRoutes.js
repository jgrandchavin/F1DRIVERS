const router = require('express').Router();
const elasticsearchClient = require('../modules/common/elasticsearch/elasticsearch');


/**
 * GET : /api/setup/
 */
router.get('/', (req, res) => {

    // For each indices delete & create new index.
    elasticsearchClient.indices.forEach(async index => {

        let mappings = require('../../config/elasticsearch/mappings/' + index + '.mapping.json');

        try {
            const indexExist = await elasticsearchClient.indexExists(index);

            if (indexExist) {
                await elasticsearchClient.deleteIndex(index);
                console.log('🗑- ' + index + ' index deleted');
            }

            await elasticsearchClient.createIndex(index, mappings);

            console.log('✨ - ' + index + ' index created');
            res.status(200).json({status: 200, message: 'Setup done ! 🍾'});
        } catch (e) {

            console.error('[ ❌ Setup ' + index +  ' index failed ]');
            res.status(500).json({status: 500, message: 'Setup failed... 🤕'})
        }
    });
});

module.exports = router;