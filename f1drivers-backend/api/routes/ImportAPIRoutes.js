const router = require('express').Router();
const elasticsearchClient = require('../modules/common/elasticsearch/elasticsearch');
const driversImporter = require('../modules/drivers/DriversImporter');

/**
 * GET api/import/drivers
 */
router.get('/drivers', async (req, res) => {

    try {
        const bulk = await driversImporter.constructImportBulk();
        await elasticsearchClient.bulk(bulk);
        console.log("⤵️- Drivers imported");
        res.status(200).json({status: 200, message: 'Driver imported ! 🏎💨'});
    } catch (e) {
        console.log("❌- Drivers import failed");
        res.status(500).json({status: 500, message: 'Driver import failed... 🤕'});
    }

});

module.exports = router;