const router = require('express').Router();
const elasticsearchClient = require('../modules/common/elasticsearch/elasticsearch');

/**
 * GET : /api/setup/
 */
router.get('/drivers', async (req, res) => {

    try {
        // Construct the query
        let query = {
            "query": {
                "match": {
                    "driver.name": {
                        "query": req.headers.driver_name,
                        "fuzziness": 1
                    }
                }
            }
        };

        const elasticResponse = await elasticsearchClient.search('f1-drivers_driver', query);
        console.log('[ 🔍 Drivers ]');
        res.status(200).json(elasticResponse.hits.hits);
    } catch (e) {
        console.log('[ 🔍 Drivers failed ❌ ]');
        res.status(500).json({status: 500, message: 'Error'});
    }
});

module.exports = router;