const router = require('express').Router();

router.use('/setup', require('./routes/SetupAPIRoutes'));
router.use('/import', require('./routes/ImportAPIRoutes'));
router.use('/search', require('./routes/SearchAPIRoutes'));

module.exports = router;