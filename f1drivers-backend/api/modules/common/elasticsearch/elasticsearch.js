const elasticsearchLibrary = require('elasticsearch');

const elasticsearchClient = new elasticsearchLibrary.Client({
    host: process.env.ELASTIC_CLUSTER_HOST || 'http://localhost:9200'
});

const indices = ['f1-drivers_driver'];
const types = {'f1-drivers_driver': 'driver'};

module.exports = {

    /**
     * Project Indices
     * TODO: Find a better place for that
     */
    indices: indices,
    types: types,

    /**
     * Test connection to ElasticSearch cluster
     *
     * @returns {*}
     */
    ping: async () => {
        return elasticsearchClient.ping({
            requestTimeout: 30000
        }, error => {
            if (error) {
                console.error("ES DOWN");
            } else {
                console.log("ES WELL");
            }
        })
    },

    /**
     * Test if an index exists.
     *
     * @param indexName
     * @returns {*}
     */
    indexExists: async (indexName) => {
        return elasticsearchClient.indices.exists({
            index: indexName
        });
    },

    /**
     * Create an index.
     *
     * @param indexName
     * @param body
     * @returns {*}
     */
    createIndex: async (indexName, body) => {
        return elasticsearchClient.indices.create({
            index: indexName,
            body: body
        })
    },

    /**
     * Delete an index.
     *
     * @param indexName
     * @returns {*}
     */
    deleteIndex: async (indexName) => {
        return elasticsearchClient.indices.delete({
            index: indexName
        })
    },

    /**
     * Execute a bulk.
     *
     * @param bulk
     * @returns {*}
     */
    bulk: async (bulk) => {
        return elasticsearchClient.bulk({body: bulk});
    },

    /**
     * Search
     *
     * @param indexName
     * @param query
     * @returns {*}
     */
    search: async (indexName, query) => {
        return elasticsearchClient.search({
            index: indexName,
            body: query
        });
    }
};
