const slugify = require('slugify');
const countries = require('../../../data/countries.json');
const drivers = require('../../../data/import/drivers');
const removeAccent = require('remove-accents');

module.exports = {

    /**
     * Convert a nationality to a country code.
     *
     * @param nationality
     * @returns {Promise<*>}
     */
    getCountryCodeFromNationality: async (nationality) => {
        let countryCode = '';


        await Promise.all(countries.map(country => {
            if(slugify(country.Nationality) === slugify(nationality)) {
                countryCode = country.Code;
            }
        }));

        return slugify(countryCode, {lower: true});
    },

    /**
     * Construct Bulk.
     *
     * @returns {Promise<Array>}
     */
    constructImportBulk: async () =>{

        try {
            let bulk = [];
            await Promise.all(drivers.map(async driver => {

                try {
                    let newDriver = {
                        "driver": {
                            "name": removeAccent(driver.first_name) + ' ' + removeAccent(driver.last_name),
                            "birthday": driver.birthday,
                            "country": await module.exports.getCountryCodeFromNationality(driver.nationality),
                            "wiki": driver.wiki
                        }
                    };

                    // Add new driver to the bulk.
                    bulk.push({
                        index: {
                            _index: 'f1-drivers_driver',
                            _type: 'driver',
                        }
                    });

                    bulk.push(newDriver);
                } catch (e) {
                    console.error("[ERROR] Failed to create a bulk... 🤕")
                }
             }));

            return bulk;
        } catch (e) {
            console.error("[ERROR] Failed to construct the bulk... 🤕")
        }
    }

};