// Required constants
const express = require('express');
const dotEnv = require('dotenv');
dotEnv.config();

// ElasticSearch
require('./api/modules/common/elasticsearch/elasticsearch');

// Server initialization
const app = express();
const server = require('http').Server(app);

// Port
const port = process.env.API_PORT || 6001 ;

app.use(function (req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', process.env.FRONT_URL || 'https://f1drivers.juliengrandchavin.io');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type, driver_name');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    //res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});

// Route
app.use('/api', require('./api/APIRoutes'));

// Start server
server.listen(port, () => {
   console.log("📃- F1Drivers server started on port " + port);
});