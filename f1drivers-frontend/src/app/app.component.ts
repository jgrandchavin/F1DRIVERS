import {ChangeDetectionStrategy, Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import { DriverService } from "./services/driver.service";
import { inAnimation } from "../assets/animations/in-animation";
import { fromEvent } from "rxjs/index";
import { debounceTime, distinctUntilChanged, map } from "rxjs/internal/operators";


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css', '../../node_modules/flag-icon-css/css/flag-icon.css'],
  changeDetection: ChangeDetectionStrategy.Default,
  animations: [inAnimation]
})

export class AppComponent implements OnInit {

  // Variables
  @ViewChild('searchText') searchText: ElementRef;
  @Input() userTextSearch: string = '';
  searchResult: Array<{}>;
  displayWelcomeMessage: boolean;

  constructor (private driverService: DriverService) {
    this.searchResult = [];
    this.displayWelcomeMessage = true;

  }

  ngOnInit() {
    fromEvent(this.searchText.nativeElement,'keyup')
      .pipe(map((evt:any) => evt.target.value))
      .pipe(debounceTime(500))
      .pipe(distinctUntilChanged())
      .subscribe((text:string) => { this.searchDrivers(text)})
  }

  /**
   *  Search Drivers
   *
   * @param driverName
   */
  searchDrivers (driverName): void {
    this.driverService.search(driverName).subscribe(response => {
        this.displayWelcomeMessage = driverName === '';
        this.searchResult = Object.keys(response).map(e=> response[e]);
    });
  }
}
