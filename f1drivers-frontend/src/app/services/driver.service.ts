
import { HttpClient } from "@angular/common/http";
import { environment } from '../../environments/environment';
import { Injectable } from '@angular/core';

@Injectable()
export class DriverService {

  apiUrl = environment.apiUrl;
  http: HttpClient;

  constructor(http: HttpClient) {
    this.http = http;
  }

  /**
   * Search drivers by his name.
   *
   * @param driverName the name of the driver we search.
   * @returns {Observable<Object>} HTTP Request.
   */
  search (driverName: string) {

    return this.http.get(
      this.apiUrl + '/api/search/drivers',
      {
        headers: {
          driver_name: driverName
      }}
    );

  }

}

