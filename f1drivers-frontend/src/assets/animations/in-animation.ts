import {animate, query, style, transition, trigger} from '@angular/animations';

export const inAnimation =
  trigger('searchResult', [
    // route 'enter' transition
    transition('void => *', [
      style({
        opacity: 0,
        transform: 'translateX(-30%)'
      }),
      animate('700ms ease-in-out')
    ])
  ]);
