import Vue from 'vue';
import Router from 'vue-router';
import Home from './_views/Home.vue';
import About from './_views/About.vue';
import LoadingPage from './_views/LoadingPage.vue';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'loading-page',
      component: LoadingPage,
    },
    {
      path: '/about',
      name: 'about',
      component: About,
    },
  ],
});
