export class Driver {

    private readonly id: number;
    private readonly name: String;
    private readonly birthday: String;
    private readonly country: String;
    private readonly wiki: String;

    constructor (id: number, name: String, birthday: String, country: String, wiki: String) {

        this.id = id;
        this.name = name;
        this.birthday = birthday;
        this.country = country;
        this.wiki = wiki;

    }

    public getId () {
        return this.id;
    }

    public getName () {
        return this.name;
    }

    public getBirthday () {
        return this.birthday;
    }

    public getCountry () {
        return this.country;
    }

    public getWiki () {
        return this.wiki;
    }

}